Installation
=========

Add add environment variable or database url to parameters `app/config/parameters.yml`


`env(DATABASE_URL): mysql://root@127.0.0.1:3306/hotels`

`bin/console doctrine:schema:create`
`bin/console doctrine:fixtures:load -n`
