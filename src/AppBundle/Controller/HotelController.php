<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Hotel;
use AppBundle\Form\CalculateRequestType;
use AppBundle\Form\HotelType;
use AppBundle\Util\Calculator\Calculator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class HotelController extends Controller
{
    /**
     * @Route("/")
     * @Template
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $query = $em->createQuery("SELECT h FROM AppBundle:Hotel h");

        $hotels = $query->getResult();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return compact('pagination', 'hotels');
    }

    /**
     * @Route("/hotel/{hotel}")
     * @Template
     */
    public function showAction(Request $request, Hotel $hotel)
    {
        $form = $this->createForm(CalculateRequestType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calculateRequest = $form->getData();
            $price = (new Calculator())->calculatePrice($calculateRequest, $hotel);

            $this->addFlash('success', 'Calculated price: $'.$price);
            return $this->redirectToRoute($request->attributes->get('_route'), ['hotel' => $hotel->getId()]);
        }

        return [
            'hotel' => $hotel,
            'form' => $form->createView()
        ];
    }
}
