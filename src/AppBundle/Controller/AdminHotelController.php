<?php

namespace AppBundle\Controller;

use AppBundle\Form\HotelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/hotel")
 */
class AdminHotelController extends Controller
{
    /**
     * @Route("/add")
     * @Template
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(HotelType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hotel = $form->getData();
            $em = $this->get('doctrine')->getManager();
            $em->persist($hotel);
            $em->flush();

            $this->addFlash('success', 'Hotel has been created!');
            return $this->redirectToRoute($request->attributes->get('_route'));
        }
        return [
            'form' => $form->createView()
        ];
    }

}
