<?php

namespace AppBundle\Util\Calculator;


use AppBundle\Entity\Hotel;

class Calculator
{
    /**
     * @param CalculateRequest $calculateRequest
     * @param Hotel $hotel
     *
     * @return float
     */
    public function calculatePrice(CalculateRequest $calculateRequest, Hotel $hotel)
    {
        return $calculateRequest->getDays() * $calculateRequest->getPeopleNum() * $hotel->getPrice();
    }
}