<?php

namespace AppBundle\Util\Calculator;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Assert\Expression(
 *     "this.getFrom() < this.getTo()",
 *     message="Dates are not valid!"
 * )
 */
class CalculateRequest
{
    /**
     * @var \DateTime
     * @Assert\NotBlank()
     */
    private $from;

    /**
     * @var \DateTime
     * @Assert\NotBlank()
     */
    private $to;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     */
    private $peopleNum;

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param \DateTime $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param \DateTime $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getPeopleNum()
    {
        return $this->peopleNum;
    }

    /**
     * @param int $peopleNum
     */
    public function setPeopleNum($peopleNum)
    {
        $this->peopleNum = $peopleNum;
    }

    /**
     * @return int
     */
    public function getDays()
    {
        $days = $this->to->diff($this->from)->days;
        if (false === $days) {
            throw new \LogicException('Cannot calculate');
        }
        return $days;
    }
}