<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Hotel;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadHotelData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $amsterdam = (new Hotel())
            ->setName('Best Western Premier Hotel Couture')
            ->setPrice(mt_rand(0, 300))
            ->setAddress('Delflandlaan 15, Slotervaart, Amsterdam, Netherlands')
            ->setLng(4.841162899999972)
            ->setLat(52.3511137);
        $manager->persist($amsterdam);

        $minsk = (new Hotel)
            ->setName('Victoria & SPA Hotel Minsk')
            ->setPrice(mt_rand(0, 300))
            ->setAddress('Prospect Pobediteley 59А, Tsentralny District, Minsk, Belarus')
            ->setLng(27.52519910000001)
            ->setLat(53.9227717);
        $manager->persist($minsk);

        $kiev = (new Hotel)
            ->setName('Khreschatyk City Center Hotel')
            ->setPrice(mt_rand(0, 300))
            ->setAddress('Khreschatyk Str.14, Kiev, Ukraine')
            ->setLng(30.525201000000038)
            ->setLat(50.45137399999999);
        $manager->persist($kiev);

        $manager->flush();
    }
}