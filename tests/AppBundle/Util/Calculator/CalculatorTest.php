<?php

namespace Tests\AppBundle\Util\Calculator;
use AppBundle\Entity\Hotel;
use AppBundle\Util\Calculator\CalculateRequest;
use AppBundle\Util\Calculator\Calculator;


class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testCalculatePrice()
    {
        $calculateRequest = new CalculateRequest();
        $calculateRequest->setFrom(new \DateTime('today'));
        $calculateRequest->setTo(new \DateTime('today+2days'));
        $calculateRequest->setPeopleNum(2);

        $hotel = new Hotel();
        $hotel->setPrice(20.20);

        $calculator = new Calculator();
        $this->assertEquals(80.80, $calculator->calculatePrice($calculateRequest, $hotel));
    }
}
